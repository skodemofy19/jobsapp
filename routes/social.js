var express = require('express');
var util = require('./util');
var http = require('http');
var router = express.Router();



/*
{
   "PostJobToTwitter":{
      "jobTitle":"Technical Marketing - Agile Integration",
      "jobDescription":"Work on 4 demos in 2 weeks",
      "jobURL":"http://github.com/weimeilin79"
   }
}
*/
router.post('/tweetjob', function(req, res, next) {

  console.log(req.body);
  //var post_data=req.body.toString();
  var post_data=req.body;
  console.log(JSON.stringify(post_data));
  var options = {
    host: 'hireinasia-app-fuseignite.52.230.124.252.nip.io',
    path: '/hireinasia/tweetjob/',
    method: 'POST',
    port: 80,
    headers: {
        'Content-Type': 'application/json'
    }
  };

  var post_req = http.request(options, function(reply){
    console.log(Object.keys(reply));
    console.log(reply.statusMessage.toString());
    res.sendStatus(200);
  });

  post_req.on('error', function(e) {
    console.error(e);
  });
  post_req.write(JSON.stringify(post_data));
  post_req.end();

});

router.post('/createcard', function(req, res, next) {

  console.log(req.body);
  //var post_data=req.body.toString();
  var post_data=req.body;
  console.log(JSON.stringify(post_data));
  var options = {
    host: 'hireinasia-app-fuseignite.52.230.124.252.nip.io',
    path: '/hireinasia/createcard/',
    method: 'POST',
    port: 80,
    headers: {
        'Content-Type': 'application/json'
    }
  };

  var post_req = http.request(options, function(reply){
    console.log(Object.keys(reply));
    console.log(reply.statusMessage.toString());
    res.sendStatus(200);
  });

  post_req.on('error', function(e) {
    console.error(e);
  });
  post_req.write(JSON.stringify(post_data));
  post_req.end();

});
module.exports = router;
