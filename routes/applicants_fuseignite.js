var express = require('express');
var util = require('./util');
var http = require('http');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
var data =null;
  util.connectionPool.request() // or: new sql.Request(pool1)
  .query('select * from APPLICANTS', (err, result) => {
      console.dir(data);
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        console.dir(result.recordsets)
        data=result.recordsets[0];
        console.dir(data);
        res.send(data);
//        res.sendStatus(200);
      }
  })
});

router.get('/(:applicant_id)', function(req, res, next){

  util.connectionPool.request() // or: new sql.Request(pool1)
  .query('select * from APPLICANTS WHERE ID = ' + req.params.applicant_id, (err, result) => {

          if (err) {
            console.log(err);
            res.sendStatus(500);
          } else {
            console.dir(result.recordsets)
            data=result.recordsets[0];
            console.dir(data);
            res.send(data);
    //        res.sendStatus(200);
          }
  })

});


router.get('/applicationByJobID/(:job_id)', function(req, res, next){

  util.connectionPool.request() // or: new sql.Request(pool1)
  .query('SELECT APPLICANTS.* FROM APPLICANTS INNER JOIN JOBS ON APPLICANTS.JOB_ID=JOBS.ID where JOB_ID =' + req.params.job_id, (err, result) => {

          if (err) {
            console.log(err);
            res.sendStatus(500);
          } else {
            console.dir(result.recordsets)
            data=result.recordsets[0];
            console.dir(data);
            res.send(data);
    //        res.sendStatus(200);
          }
  })

});


/*
INSERT INTO APPLICANTS (ID, NAME, EXPECTED_SALARY, PHOTO_LINK, STATUS, RESUME, JOB_ID)

{
  "ID": NULL`,
  "NAME": "john",
  "SALARY_ASK": 12000,
  "PHOTO_URL": "http://test.com/123.png",
  "STATUS" :"PENDING",
  "RESUME" : "this is my CV",
   "JOB_ID": 001
}

*/

router.post('/', function(req, res, next) {

  console.log(req.body);
//  var q='Update applicants SET first_name="'+req.param('first_name')+'", last_name="'+req.param('last_name')+'", phone="'+req.param('phone')+'", email="'+req.param('email')+'" where applicant_id="'+req.param('applicant_id')+'"';
  var appl=req.body;
//var q="Insert into APPLICANTS (ID,NAME,SALARY_ASK, PHOTO_URL,STATUS,RESUME,JOB_ID,ROWID) values (((SELECT MAX(ID) from APPLICANTS)+1),'"+appl.NAME+"',"+appl.SALARY_ASK+",'"+appl.PHOTO_URL+"','"+appl.STATUS+"','"+appl.RESUME+"',"+appl.JOB_ID+",NEWID())";
var q="Insert into APPLICANTS (NAME,SALARY_ASK, PHOTO_URL,STATUS,RESUME,JOB_ID) values ('"+appl.NAME+"',"+appl.SALARY_ASK+",'"+appl.PHOTO_URL+"','"+appl.STATUS+"','"+escape(appl.RESUME)+"',"+appl.JOB_ID+")";

//var q="Insert into APPLICANTS values ('"+appl.NAME+"',"+appl.SALARY_ASK+",'"+appl.PHOTO_URL+"','"+appl.STATUS+"','"+appl.RESUME+"',"+appl.JOB_ID+")";
console.log(q);
  util.connectionPool.request() // or: new sql.Request(pool1)
  .query(q, (err, result) => {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      console.log(result.rowsAffected+' rows created');
      if (result.rowsAffected == 0) {
        res.send('records not created');
      }
      else {
         //get job info
         var job=null;
         var options = {
           host: 'localhost',
           port: process.env.PORT || 8080,
           path: '/jobs/'+appl.JOB_ID
         };

         var get_req = http.get(options, function(get_res) {
           console.log('STATUS: ' + get_res.statusCode);
           console.log('HEADERS: ' + JSON.stringify(get_res.headers));

           // Buffer the body entirely for processing as a whole.
           var bodyChunks = [];
           get_res.on('data', function(chunk) {
             // You can process streamed parts here...
             bodyChunks.push(chunk);
           }).on('end', function() {
             job = JSON.parse(Buffer.concat(bodyChunks))[0];
             console.log('BODY: ' + job);
             console.log('JOBID: ' + job.ID);
             var t_description="#"+job.TITLE+"#\n**Location :** "+job.LOCATION+"\n**Expected Salary :** US$"+appl.SALARY_ASK+"\n**Resume**\n\n--- \n\n"+appl.RESUME;
             var trello={
            	"CreateCardInTrello":
            	{
            			"TITLE": job.TITLE,
            			"LOCATION": job.LOCATION,
            			"CATEGORY" : job.CATEGORY,
            			"TYPE" : job.TYPE,
            			"DESCRIPTION" : job.DESCRIPTION,
            			"URL" : job.URL,
            			"JOB_ID": (job.ID).toString(),
            			"NAME": appl.NAME,
            			"SALARY_ASK": (appl.SALARY_ASK).toString(),
            			"PHOTO_URL": appl.PHOTO_URL,
            			"RESUME": appl.RESUME,
            			"STATUS": appl.STATUS
            	}
            };

            //save to trekko
            console.log(JSON.stringify(trello));
            var options = {
              host: 'api.trello.com',
              path: '/1/cards',
              method: 'POST',
              port: 443,
              headers: {
                  'Content-Type': 'application/json'
              }
            };

            // var options = {
            //   host: 'hireinasia-app-fuseignite.52.230.124.252.nip.io',
            //   path: '/hireinasia/createcard/',
            //   method: 'POST',
            //   port: 80,
            //   headers: {
            //       'Content-Type': 'application/json'
            //   }
            // };

            var post_req = http.request(options, function(reply){
              console.log(Object.keys(reply));
              console.log(reply.statusMessage.toString());
              res.sendStatus(200);
            });

            post_req.on('error', function(e) {
              console.error(e);
            });
            post_req.write(JSON.stringify(trello));
            post_req.end();


           })
         });

         get_req.on('error', function(e) {
           console.log('ERROR: ' + e.message);
         });


        //res.sendStatus(200);
      }
    }
  });
});

router.post('/(:applicantID)', function(req, res, next) {

  console.log(req.body);
//  var q='Update applicants SET first_name="'+req.param('first_name')+'", last_name="'+req.param('last_name')+'", phone="'+req.param('phone')+'", email="'+req.param('email')+'" where applicant_id="'+req.param('applicant_id')+'"';
  var appl=req.body;
var q="Update APPLICANTS SET SALARY_ASK="+appl.SALARY_ASK+", NAME='"+appl.NAME+"', PHOTO_URL='"+appl.PHOTO_URL+"', STATUS='"+appl.STATUS+"', RESUME='"+appl.RESUME+"', JOB_ID='"+appl.JOB_ID+"' where ID='"+req.param("applicantID")+"'";
console.log(q);
  util.connectionPool.request() // or: new sql.Request(pool1)
  .query(q, (err, result) => {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      console.log(result.rowsAffected+' rows updated');
      if (result.rowsAffected == 0) {
        res.send('records not found');
      }
      else
      res.sendStatus(200);
    }
  });
});

router.delete('/(:applicantID)', function(req, res, next) {


    util.connectionPool.request() // or: new sql.Request(pool1)
    .query("DELETE FROM APPLICANTS WHERE ID = '" + req.params.applicantID+"'", (err, result) => {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        console.log(result.rowsAffected+' rows deleted');
        if (result.rowsAffected == 0) {
          res.send('records not found');
        }
        else
        res.sendStatus(200);
      }

    });

})
module.exports = router;
