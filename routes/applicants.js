var express = require('express');
var util = require('./util');
var http = require('http');
var https = require('https');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
var data =null;
  util.connectionPool.request() // or: new sql.Request(pool1)
  .query('select * from APPLICANTS', (err, result) => {
      console.dir(data);
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        console.dir(result.recordsets)
        data=result.recordsets[0];
        console.dir(data);
        res.send(data);
//        res.sendStatus(200);
      }
  })
});

router.get('/(:applicant_id)', function(req, res, next){

  util.connectionPool.request() // or: new sql.Request(pool1)
  .query('select * from APPLICANTS WHERE ID = ' + req.params.applicant_id, (err, result) => {

          if (err) {
            console.log(err);
            res.sendStatus(500);
          } else {
            console.dir(result.recordsets)
            data=result.recordsets[0];
            console.dir(data);
            res.send(data);
    //        res.sendStatus(200);
          }
  })

});


router.get('/applicationByJobID/(:job_id)', function(req, res, next){

  util.connectionPool.request() // or: new sql.Request(pool1)
  .query('SELECT APPLICANTS.* FROM APPLICANTS INNER JOIN JOBS ON APPLICANTS.JOB_ID=JOBS.ID where JOB_ID =' + req.params.job_id, (err, result) => {

          if (err) {
            console.log(err);
            res.sendStatus(500);
          } else {
            console.dir(result.recordsets)
            data=result.recordsets[0];
            console.dir(data);
            res.send(data);
    //        res.sendStatus(200);
          }
  })

});


/*
INSERT INTO APPLICANTS (ID, NAME, EXPECTED_SALARY, PHOTO_LINK, STATUS, RESUME, JOB_ID)

{
  "ID": NULL`,
  "NAME": "john",
  "SALARY_ASK": 12000,
  "PHOTO_URL": "http://test.com/123.png",
  "STATUS" :"PENDING",
  "RESUME" : "this is my CV",
   "JOB_ID": 001
}

*/

router.post('/', function(req, res, next) {

  console.log(req.body);
//  var q='Update applicants SET first_name="'+req.param('first_name')+'", last_name="'+req.param('last_name')+'", phone="'+req.param('phone')+'", email="'+req.param('email')+'" where applicant_id="'+req.param('applicant_id')+'"';
  var appl=req.body;
  appl.PHOTO_URL='http://imagehost-skodemo2018.52.230.124.252.nip.io/images/Paul.jpg';
//var q="Insert into APPLICANTS (ID,NAME,SALARY_ASK, PHOTO_URL,STATUS,RESUME,JOB_ID,ROWID) values (((SELECT MAX(ID) from APPLICANTS)+1),'"+appl.NAME+"',"+appl.SALARY_ASK+",'"+appl.PHOTO_URL+"','"+appl.STATUS+"','"+appl.RESUME+"',"+appl.JOB_ID+",NEWID())";
//ok
var q="Insert into APPLICANTS (NAME,SALARY_ASK, PHOTO_URL,STATUS,RESUME,JOB_ID) values ('"+appl.NAME+"',"+appl.SALARY_ASK+",'"+appl.PHOTO_URL+"','"+appl.STATUS+"','"+escape(appl.RESUME)+"',"+appl.JOB_ID+")";
//http://imagehost-skodemo2018.52.230.124.252.nip.io/images/Paul.jpg
//hardcode
//var q="Insert into APPLICANTS (NAME,SALARY_ASK, PHOTO_URL,STATUS,RESUME,JOB_ID) values ('"+appl.NAME+"',"+appl.SALARY_ASK+",'','"+appl.STATUS+"','"+escape(appl.RESUME)+"',"+appl.JOB_ID+")";
//var q="Insert into APPLICANTS values ('"+appl.NAME+"',"+appl.SALARY_ASK+",'"+appl.PHOTO_URL+"','"+appl.STATUS+"','"+appl.RESUME+"',"+appl.JOB_ID+")";
console.log(q);
  util.connectionPool.request() // or: new sql.Request(pool1)
  .query(q, (err, result) => {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      console.log(result.rowsAffected+' rows created');
      if (result.rowsAffected == 0) {
        res.send('records not created');
      }
      else {
         //get job info
         var job=null;
         var options = {
           host: 'localhost',
           port: process.env.PORT || 8080,
           path: '/jobs/'+appl.JOB_ID
         };

         var get_req = http.get(options, function(get_res) {
           console.log('STATUS: ' + get_res.statusCode);
           console.log('HEADERS: ' + JSON.stringify(get_res.headers));

           // Buffer the body entirely for processing as a whole.
           var bodyChunks = [];
           get_res.on('data', function(chunk) {
             // You can process streamed parts here...
             bodyChunks.push(chunk);
           }).on('end', function() {
             job = JSON.parse(Buffer.concat(bodyChunks))[0];
             console.log('BODY: ' + job);
             console.log('JOBID: ' + job.ID);

             //call trello
//             var description="#"+job.TITLE+"#\n**Location :** "+job.LOCATION+"\n**Expected Salary :** US$"+appl.SALARY_ASK+"\n**Resume**\n---\n"+appl.RESUME;

              //hardcoded
             var description="#"+job.TITLE+"#\n**Location :** "+job.LOCATION+"\n**Expected Salary :** US$"+appl.SALARY_ASK+"\n**Resume**\n\n--- \n\nPaul Cormier leads Red Hat's technology and products organizations, including engineering, product management, and product marketing for Red Hat's technologies. He joined Red Hat in May 2001 as executive vice president, Engineering.\n\nCormier's leadership and experience in enterprise technology has led to the introduction of Red Hat's acclaimed line of enterprise products, including Red Hat Enterprise Linux®. \n\nPaul is highly skilled in the native language of Boston.\n\n\n--- \n\n\n";


             //var description="#"+job.TITLE+"#\n**Location :** "+job.LOCATION+"\n**Expected Salary :** US$"+appl.SALARY_ASK+"\n**Resume**\n\n--- \n\n"+unescape(appl.RESUME);
             var cardInfo=
             {
             	"key": "41acc9e1c7e215685f6ee3aca7de88b0",
             	"token": "5fc357e404c37037b8a510587ab29425acf3b79ecd2018bc740b697ede81b488",
             	"desc": description,
             	"name":appl.NAME,
             	"due": null,
              "pos": "top",
             	"idList":"5a9e17341917329e13af2954",
              "id": null,
              "attachment_id":null
            };
            var card_options = {
              host: 'api.trello.com',
              path: '/1/cards',
              method: 'POST',
              port: 443,
              headers: {
                  'Content-Type': 'application/json'
              }
            };


            var post_req = https.request(card_options, function(reply){
              console.log(Object.keys(reply));
              console.log(reply.statusMessage.toString());
              var cardReply = [];
              reply.on('data', function(chunk) {
                // You can process streamed parts here...
                cardReply.push(chunk);
                //console.log(chunk);
              }).on('end', function() { //createcard
                var c= JSON.parse(Buffer.concat(cardReply))
                console.log(c.id);
                cardInfo.id=c.id;

                //upload attachment
                //
                var attachmentInfo=
                {
                  "url": appl.PHOTO_URL,
                  "key": "41acc9e1c7e215685f6ee3aca7de88b0",
                  "token": "5fc357e404c37037b8a510587ab29425acf3b79ecd2018bc740b697ede81b488"
                }
                var attach_options = {
                  host: 'api.trello.com',
                  path: '/1/cards/'+c.id+'/attachments',
                  method: 'POST',
                  port: 443,
                  headers: {
                      'Content-Type': 'application/json'
                  }
                };


                var attach_req = https.request(attach_options, function(attach_reply){
                  console.log(Object.keys(attach_reply));
                  console.log(attach_reply.statusMessage.toString());
                  var attachData = [];
                  attach_reply.on('data', function(chunk) {
                    // You can process streamed parts here...
                    attachData.push(chunk);
                    //console.log(chunk);
                  }).on('end', function() { //attach
                    var d= JSON.parse(Buffer.concat(attachData))
                    console.log(d.id);
                    cardInfo.attachment_id=d.id;
                  });//on end attachment


                });//attach_req
                attach_req.on('error', function(e) {
                  console.error(e);
                });
                attach_req.write(JSON.stringify(attachmentInfo));
                attach_req.end();

                 //end upload attachment




             });//createcard on end

              res.sendStatus(200);
            });//get job info

            post_req.on('error', function(e) {
              console.error(e);
            });
            post_req.write(JSON.stringify(cardInfo));
            post_req.end();


          })//on end
         });

         get_req.on('error', function(e) {
           console.log('ERROR: ' + e.message);
         });


        //res.sendStatus(200);
      }
    }
  });
});

router.post('/(:applicantID)', function(req, res, next) {

  console.log(req.body);
//  var q='Update applicants SET first_name="'+req.param('first_name')+'", last_name="'+req.param('last_name')+'", phone="'+req.param('phone')+'", email="'+req.param('email')+'" where applicant_id="'+req.param('applicant_id')+'"';
  var appl=req.body;
var q="Update APPLICANTS SET SALARY_ASK="+appl.SALARY_ASK+", NAME='"+appl.NAME+"', PHOTO_URL='"+appl.PHOTO_URL+"', STATUS='"+appl.STATUS+"', RESUME='"+appl.RESUME+"', JOB_ID='"+appl.JOB_ID+"' where ID='"+req.param("applicantID")+"'";
console.log(q);
  util.connectionPool.request() // or: new sql.Request(pool1)
  .query(q, (err, result) => {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      console.log(result.rowsAffected+' rows updated');
      if (result.rowsAffected == 0) {
        res.send('records not found');
      }
      else
      res.sendStatus(200);
    }
  });
});

router.delete('/(:applicantID)', function(req, res, next) {


    util.connectionPool.request() // or: new sql.Request(pool1)
    .query("DELETE FROM APPLICANTS WHERE ID = '" + req.params.applicantID+"'", (err, result) => {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        console.log(result.rowsAffected+' rows deleted');
        if (result.rowsAffected == 0) {
          res.send('records not found');
        }
        else
        res.sendStatus(200);
      }

    });

})
module.exports = router;
