var app={
  createJob: function() {

    var job={
      "ID": null,
      "TITLE": $('#cj_title').val(),
      "LOCATION": $('#cj_loc option:selected').text(),
      "CATEGORY": $('#cj_category').val(),
      "TYPE" :$('#cj_type').val(),
      "DESCRIPTION": $('#cj_desc').val(),
      //"URL": $('#cj_url').val()
      "URL" : "http://jobsapp-api-skodemo2018.52.230.124.252.nip.io/apply.html"
    };
    console.log(job.TITLE);
    $.ajax({
      type: "POST",
      url: '/JOBS',
      data: job,
      //data: JSON.stringify(job),
      //success: app.renderJobs,
      dataType: 'JSON'
    });


  },
  loadJobs: function() {

    $.ajax({
      type: "GET",
      url: '/JOBS',
//        data: data,
      success: app.renderJobs,
      dataType: 'JSON'
    });

  },
  renderJobs: function(data){
    console.log(Object.keys(data[0]));
    for (var i =0 ; i< data.length;i++) {
      console.log(data[i].TITLE);
      $('#jobsTable').find('tbody').append('<tr><td>'+data[i].TITLE+'</td><td>'+data[i].TYPE+'</td><td>'+data[i].LOCATION+'</td><td>'+data[i].DESCRIPTION+'</td></tr>');
      $('#available-jobs-table').find('tbody').append('<tr><td>'+data[i].TITLE+'</td><td>'+data[i].TYPE+'</td><td>'+data[i].LOCATION+'</td><td>'+data[i].DESCRIPTION+'</td><td><'+data[i].URL+'</td></tr>');
//      $('#jobsTable').find('tbody').append('<tr><td>'+data[i].TITLE+'</td><td>'+data[i].CATEGORY+'</td><td>'+data[i].TYPE+'</td><td>'+data[i].LOCATION+'</td><td>'+data[i].DESCRIPTION+'</td><td>'+data[i].URL+'</td></tr>');
//      $('#available-jobs-table').find('tbody').append('<tr><td>'+data[i].TITLE+'</td><td>'+data[i].CATEGORY+'</td><td>'+data[i].TYPE+'</td><td>'+data[i].LOCATION+'</td><td>'+data[i].DESCRIPTION+'</td><td>'+data[i].URL+'</td></tr>');
    }
  },
  loadApplicants: function() {

    $.ajax({
      type: "GET",
      url: '/APPLICANTS',
//        data: data,
      success: app.renderApplicants,
      dataType: 'JSON'
    });

  },
  renderApplicants: function(data){
    console.log(Object.keys(data[0]));
    for (var i =0 ; i< data.length;i++) {
      $('#applicantsTable').find('tbody').append('<tr><td> <div class="ava" style="background-image: url('+data[i].PHOTO_URL+')" >&nbsp</div>   </td><td>'+data[i].NAME+'</td><td>'+data[i].SALARY_ASK+'</td><td>'+data[i].STATUS+'</td><td>'+unescape(data[i].RESUME)+'</td></tr>');
//      $('#applicantsTable').find('tbody').append('<tr><td><img src="'+data[i].PHOTO_URL+'"/></td><td>'+data[i].NAME+'</td><td>'+data[i].ID+'</td><td>'+data[i].SALARY_ASK+'</td><td>'+data[i].STATUS+'</td><td>'+unescape(data[i].RESUME)+'</td></tr>');

    }
  },
  createApplicant: function(){
      var applicant={
        "ID": null,
        "NAME": $('#ap_name').val(),
        "STATUS": 'PENDING',
        "SALARY_ASK": $('#ap_salary').val(),
        "RESUME" :$('#ap_resume').val(),
        "JOB_ID": $('#ap_jobsel').val(),
        "PHOTO_URL": $('#ap_photo').val()
      };
      console.log(applicant.NAME);
      $.ajax({
        type: "POST",
        url: '/APPLICANTS',
        data: applicant,
        //data: JSON.stringify(job),
        success: app.loadApplicants,
        dataType: 'JSON'
      });
  },
  registerEvents:function() {
    $('input[type=radio][name=switch]').change(function (event) {
      console.log(event.currentTarget.id);
      if (event.currentTarget.id=='available-jobs') {
        app.cleanJobTable();
        app.loadJobs();
      }//if
      else if (event.currentTarget.id=='applicants-list') {
        app.cleanApplicantsTable();
        app.loadApplicants();
      }//if
      else if (event.currentTarget.id=='available-jobs') {
        app.cleanJobTable();
        app.loadJobs();
      }//if
      else if (event.currentTarget.id=='jobs-list') {
        app.cleanJobTable();
        app.loadJobs();
      }//if
      else if (event.currentTarget.id=='create-job') {
        $('#cj_title').val('');
        $('#cj_type').val('');
        $('#cj_desc').val('');
        $('#cj_loc').val('');
        $('#cj_category').val('');
        $('#cj_url').val('');
      }//if
      else if (event.currentTarget.id=='apply') {

        //console.log('apply job '+$('#ap_jobsel').val()+ ' '+$('#ap_jobsel option:selected').text());
        // $('#ap_jobsel').val('');
         $('#ap_name').val('');
         $('#ap_salary').val('');
         $('#ap_resume').val('');
         $('#ap_photo').val('');
         $('#ap_jobsel').empty();
         $('#ap_jobsel').append($('<option>', {
             value: '',
             text : ''
         }));
         //load list
         $.ajax({
           type: "GET",
           url: '/JOBS',
     //        data: data,
           success: function(items) {
             console.log(items.length);
             $.each(items, function (i, item) {
                 $('#ap_jobsel').append($('<option>', {
                     value: item.ID,
                     text : item.TITLE
                 }));
             });
           },
           dataType: 'JSON'
         });



      }//if
      else if (event.currentTarget.id=='application-success') {
        console.log('application-success');
      }
    });//swtich events
    $('.button').on('click',function (event) {

      console.log('button clicked '+event.currentTarget.id);
      if (event.currentTarget.id == 'btn_cj') {
        console.log('create Job method');
        app.createJob();
      } else if ( event.currentTarget.id =='btn_ap'){
        app.createApplicant();
        console.log('change page');
        $('#application-success').prop('checked', true);
      }
    });
  },
  cleanJobTable:function(){
    $('#jobsTable').find('tbody').empty();
    $('#available-jobs-table').find('tbody').empty();

  },
  cleanApplicantsTable:function(){
    $('#applicantsTable').find('tbody').empty();
  }
};
/*
<input type="radio" name="switch" id="jobs-list" checked/>
<input type="radio" name="switch" id="applicants-list"  />
<input type="radio" name="switch" id="create-job"  />
<!-- visitors -->
<input type="radio" name="switch" id="available-jobs" />
<input type="radio" name="switch" id="apply"  />
<input type="radio" name="switch" id="application-success"  />

*/
